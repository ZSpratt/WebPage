fst = "TestContent1";
lst = "TestContent4";
fullURL = window.location.href;
baseURL = fullURL.split("?")[0];
argURL = fullURL.split("?")[1];
function Next(url) {
	if (url != undefined){
		document.getElementById('Next').setAttribute("href", baseURL + "?" + url);
	} else {
		document.getElementById('Next').childNodes[0].className = "inactiveButton"
	}
}

function Last(url) {
	if (url != undefined){
		document.getElementById('Last').setAttribute("href", baseURL + "?" + url)
	} else {
		document.getElementById('Last').childNodes[0].className = "inactiveButton"
	}
}

function Prev(url) {
	if (url != undefined){
		document.getElementById('Prev').setAttribute("href", baseURL + "?" + url);
	} else {
		document.getElementById('Prev').childNodes[0].className = "inactiveButton"
	}
}

function Firs(url) {
	if (url != undefined){
		document.getElementById('Firs').setAttribute("href", baseURL + "?" + url);
	} else {
		document.getElementById('Firs').childNodes[0].className = "inactiveButton"
	}
}

function LoadContent() {
	if (argURL == undefined){
		$("#LinkSetup").load(lst + "/Link.html");
		$("#MainContent").load(lst + "/Content.html");
		$("#ContentTitle").load(lst + "/Title.html");
		$("#Description").load(lst + "/Description.html");
		
	} else {
		$("#LinkSetup").load(argURL + "/Link.html");
		$("#MainContent").load(argURL + "/Content.html");
		$("#ContentTitle").load(argURL + "/Title.html");
		$("#Description").load(argURL + "/Description.html");
	}
}

console.log(argURL)
if (fst != argURL) {
	Firs(fst);
} else {
	Firs(undefined)
}

if (lst != argURL && argURL != undefined) {
	Last(lst);
} else {
	Last(undefined)
}

LoadContent();

