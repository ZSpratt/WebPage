RUN INSTRUCTIONS:
	python3 WebWebpage.py

Requirements:
	Python3
	packages:
		Flask
		PIL
		sqlite3

Runs a local web server, using port 5000.
This website displays posts based on a database, and allows for comments, view tracking, and uploading of new posts and header image, allong with a basic user management system.
Comments are made safe by ignoring html tags placed inside.
Anonymous users can comment and view posts, but not edit profile.

In order to access uploading use the following login credentials.

ZSpratt
password
(No, this isnt my real password anywhere but here.  It will let you in to upload stuff to your own copy of the site)