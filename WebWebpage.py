from flask import Flask, render_template, current_app, request, redirect, abort, flash
from flask_login import LoginManager, login_user, logout_user, current_user
from PostListing import PostListing
import os
import sys
import re
from random import choice, random
from PIL import Image
from WebDatabase import *

realpath = os.path.dirname(os.path.relpath(__file__))
if realpath != '':
    realpath += "/"
app = Flask(__name__)
app.secret_key = "ZSpratt-arts.comKingSiteManWithGun"
login_manager = LoginManager()
login_manager.init_app(app)

postList = getPostList()


@login_manager.user_loader
def load_user(user_id):
    return getUser(user_id)


def randomHeader():
    headersPath = "./{}/static/Headers/".format(os.path.dirname(os.path.relpath(__file__)))
    header = "static/Headers/" + choice(os.listdir(headersPath))
    return header


@app.route("/", methods=['GET', 'POST'])
def Home():
    return Post()


@app.route("/Archive")
def Archive():
    currentPage = request.args.get('page')
    if currentPage == None:
        currentPage = 0
    else:
        currentPage = int(currentPage)
    postList = getPostList(start = currentPage, limit = 5)
    archiveList = []
    pages = list(range(0, postCount(), 5))
    print(currentPage)

    for post in postList:
        title = open(realpath + post[2], "r").read()
        content = open(realpath + post[3], "r").read()
        desc = open(realpath + post[4], "r").read()
        archiveList.append((post[1], title, content, desc))
    return render_template("PostList.html", pages = pages, currentPage = currentPage, list=archiveList, header=randomHeader())


@app.route("/Search", methods=['GET', 'POST'])
def Search():

    searchResults = []

    if "tags" in request.form:
        search = request.form["tags"]
    else:
        search = request.args.get('search')

    tags = search.replace(",", " ").split(" ")
    print("The tags " + str(tags))
    postList = searchPosts(tags)
    for post in postList:
        title = open(realpath + post[1], "r").read()
        content = open(realpath + post[2], "r").read()
        desc = open(realpath + post[3], "r").read()
        searchResults.append((post[0], title, content, desc))

    if (len(searchResults)) == 0:
        searchResults = None

    currentPage = request.args.get('page')

    if currentPage == None:
        currentPage = 0
    else:
        currentPage = int(currentPage)
    print(currentPage)
    pages = []
    if (searchResults is not None):
        pages = list(range(0, len(searchResults), 5))
        searchResults = searchResults[currentPage : currentPage + 5]

    return render_template("PostList.html", search=True, lastSearch = search, pages = pages, currentPage = currentPage, list=searchResults, header=randomHeader())


@app.route("/Games")
def Games():
    return render_template("Games.html", header=randomHeader())


@app.route("/Commissions")
def Commissions():
    return render_template("Commissions.html", header=randomHeader())


@app.route("/Register", methods=['GET', 'POST'])
def Register():
    err = ""
    if request.method == "POST":
        if request.form["type"] == "register":
            if request.form["pass"] == request.form["pass1"]:
                err = register(request.form["name"], request.form["pass"], request.form["email"], "useGravatar" in request.form)
                if err is None:
                    login_user(getUser(request.form["name"]), remember=True)
            else:
                err = "Passwords do not match"
    return render_template("Register.html", error=err, header=randomHeader())


@app.route('/login', methods=['GET', 'POST'])
def Login():
    redirect_to_index = redirect(request.form["dest"])
    response = current_app.make_response(redirect_to_index)
    name = request.form["name"]
    pas = request.form["pass"]
    if login(name, pas):
        login_user(getUser(name), remember=True)
    return response

@app.route('/updateUser', methods=['GET', 'POST'])
def UpdateUser():
    redirect_to_index = redirect("/User")
    response = current_app.make_response(redirect_to_index)
    name = request.form["name"]
    pas = request.form["pass"]
    email = request.form["email"]
    email.strip().replace(" ", "")
    if login(name, pas):
        updateUser(name, pas, email, request.form.get('useGravatar'))
    return response

@app.route('/loginAnon', methods=['GET', 'POST'])
def LoginAnon():
    #print(request.form)
    redirect_to_index = redirect(request.form["dest"])
    response = current_app.make_response(redirect_to_index)
    name = "Anonymous"

    login_user(getUser(name), remember=True)
    return response


@app.route('/logout', methods=['GET', 'POST'])
def Logout():
    redirect_to_index = redirect(request.form["dest"])
    response = current_app.make_response(redirect_to_index)
    name = request.form["name"]
    logout_user()
    if name != "Anonymous":
        login_user(getUser("Anonymous"), remember=True)
    return response


@app.route("/EditPost", methods=['GET', 'POST'])
def EditPost():
    tagList = getTags(request.form["post"])
    #print(tagList)
    #print(request.form["post"])
    post = getPost(request.form["post"])
    filesO = os.listdir(realpath + post[2][:-10])
    files = []
    for fil in filesO:
        if not ".html" in fil:
            files.append(fil)
    return render_template("EditPost.html", post=post[1], files=files, title=open(realpath + post[2]).read(), content=open(realpath + post[4]).read(), description=open(realpath + post[3]).read(), tags=tagList, header=randomHeader())


@app.route('/submitEdit', methods=['GET', 'POST'])
def submitEdit():
    redirect_to_index = redirect(request.form["dest"])
    if request.form["cCon"].count("\"") % 2 == 0 and request.form["cCon"].count("\'") % 2 == 0:
        flash(updatePost(request.form["post"], request.form["title"], request.form["cCon"], request.form["desc"], request.form["tags"].replace(",", " ").split(" ")))
    else:
        flash("Edit Failed")
    response = current_app.make_response(redirect_to_index)
    return response

@app.route("/AddHeader", methods=['GET', 'POST'])
def AddHeader():
    error = ""
    if request.method == "POST":
        files = request.files.getlist("images")
        if len(files) == 0:
            error = "No Files"
            return render_template("CreatePost.html", error=error, header=randomHeader())
        destPath = realpath + "static/Headers/"
        for f in files:
            if not os.path.isfile(destPath + f.filename):
                print("save " + f.filename)
                f.save(destPath + "{}".format(f.filename))
            else:
                error += "{} exists<br>".format(f.filename)

    if current_user.is_authenticated and current_user.id == "ZSpratt":
        return render_template("AddHeader.html", error=error, header=randomHeader())
    else:
        return abort(404)

@app.route("/CreatePost", methods=['GET', 'POST'])
def CreatePost():
    error = ""
    if request.method == "POST":
        dest = request.form["dest"]
        title = request.form["title"]
        files = request.files.getlist("images")
        smFiles = []
        content = request.form["cCon"]
        desc = request.form["desc"]

        if current_user.is_authenticated and current_user.id == "ZSpratt":
            if title == "":
                flash("NoTitle")
                return render_template("CreatePost.html", error=error, header=randomHeader())
            if len(files) == 0:
                flash("NoFiles")
                return render_template("CreatePost.html", error=error, header=randomHeader())
            if desc == "":
                flash("NoDesc")
                return render_template("CreatePost.html", error=error, header=randomHeader())
            if dest == "":
                dest = title.replace(" ", "")

            destPath = realpath + "static/Posts/{}/".format(dest)
            while os.path.exists(destPath):
                dest += "C"
                destPath = realpath + "static/Posts/{}/".format(dest)
            os.makedirs(destPath)
            for f in files:
                print("save " + f.filename)
                f.save(destPath + "{}".format(f.filename))
                print("load " + f.filename)
                img = Image.open(destPath + "{}".format(f.filename))
                w = int(img.size[0] * 0.25)
                h = int(img.size[1] * 0.25)
                img = img.resize((w, h), Image.ANTIALIAS)
                img.save(destPath + "/small {}".format(f.filename))

            if content == "":
                i = 0
                for f in files:
                    i += 1
                    content += '<image src="static/Posts/{}/small {}" onclick="window.location = \'static/Posts/{}/{}\';"/>'.format(dest, f.filename, dest, f.filename)
                    if i % (len(files) / 2) == 0:
                        content += "<br>\n"
            else:
                i = 0
                print(content)
                for f in files:
                    print(f.filename)
                    content = str.replace(content, "${}".format(i), 'static/Posts/{}/small {}" onclick="window.location = \'static/Posts/{}/{}\';"'.format(dest, f.filename, dest, f.filename))
                    i += 1
                print(content)
            print(destPath + "Title.html".format(dest))
            titleF = open(destPath + "Title.html".format(dest), "w")
            titleF.write(title)
            titleF.close()

            print(destPath + "Content.html".format(dest))
            contentF = open(destPath + "Content.html".format(dest), "w")
            contentF.write(content)
            contentF.close()

            print(destPath + "Description.html".format(dest))
            descF = open(destPath + "Description.html".format(dest), "w")
            descF.write(desc)
            descF.close()

            addPost(dest, request.form["tags"].split(","))
            error = "post success"
            #print("{} {} {} {}".format(str(title), str(files), str(content), str(desc)))
    if current_user.is_authenticated and current_user.id == "ZSpratt":
        return render_template("CreatePost.html", error=error, header=randomHeader())
    else:
        return abort(404)


@app.route("/Post", methods=['GET', 'POST'])
def Post():
    #print(request.url)
    postList = getPostList()

    if request.method == "POST":
        if request.form["type"] == "comment":
            addComment(request.form)

    postName = request.args.get('PostID')
    post = len(postList) - 1
    if post == -1:
        return render_template("MissingPost.html", post="No Posts", header=randomHeader())
    if postName:
        postPos = getPost(postName)
        if postPos is not None:
            post = postPos[0] - 1
        else:
            return render_template("MissingPost.html", post=postName, header=randomHeader())
    else:
        postName = postList[post][1]
        postPos = getPost(postName)

    fileList = os.listdir(realpath + postPos[2][:-10])
    #print(fileList)
    prev = None
    for f in fileList:
        prev = "https://en.gravatar.com/userimage/34648677/30eceeeb9d74ab58b8a7b16d3c74d322.png?size=200"

    content = None
    desc = None
    title = None
    #print(realpath + postPos[2])
    if os.path.isfile(realpath + postPos[2]):
        title = open(realpath + postPos[2], "r").read()
    #print(realpath + postPos[3])
    if os.path.isfile(realpath + postPos[3]):
        desc = open(realpath + postPos[3], "r").read()
    #print(realpath + postPos[4])
    if os.path.isfile(realpath + postPos[4]):
        content = open(realpath + postPos[4], "r").read()
    links = generateLinks(post)
    #print((content, title, desc))
    addView(postName, request.remote_addr)
    views = getViews(postName)
    return render_template("Post.html", views = views, preview=prev, previewTitle=postPos[1], previewURL=request.url_root + "/Post?PostID=" + postPos[1], header=randomHeader(), content=content, title=title, desc=desc, links=links, comments=generateComments(postName), postID=postName)


@app.route("/User", methods=['GET', 'POST'])
def User():
    return render_template("User.html", header=randomHeader())

def generateLinks(post):
    links = ""
    postList = getPostList()
    if post == 0:
        links += '<a id="Firs"><img class="inactiveButton" src="static/Theme/Navigation/PrevPrev.png" width="40vmin"></a>'
        links += '<a id="Prev"><img class="inactiveButton" src="static/Theme/Navigation/Prev.png" width="40vmin"></a>'
    else:
        links += '<a id="Firs" href="{}Post?PostID={}"><img class="button" src="static/Theme/Navigation/PrevPrev.png" width="40vmin"></a>'.format(request.url_root, postList[0][1])
        links += '<a id="Firs" href="{}Post?PostID={}"><img class="button" src="static/Theme/Navigation/Prev.png" width="40vmin"></a>'.format(request.url_root, postList[post - 1][1])

    randomLink = postList.copy()
    if (len(postList)) > 1:
        del randomLink[post]
    here = postList[post]
    rndm = choice(randomLink)[1]
    links += """<div style="display: inline-block;">
              <a id="Perm" href="{}Post?PostID={}"><img class="button" src="static/Theme/Navigation/PermLink.png" width="11vmin"></a><br>
              <a id="Rndm" href="{}Post?PostID={}"><img class="button" src="static/Theme/Navigation/RandLink.png" width="11vmin"></a>
              </div>""".format(request.url_root, postList[post][1], request.url_root, choice(randomLink)[1])

    if post == len(postList) - 1:
        links += '<a id="Next"><img class="inactiveButton" src="static/Theme/Navigation/Next.png" width="40vmin"></a>'
        links += '<a id="Last"><img class="inactiveButton" src="static/Theme/Navigation/NextNext.png" width="40vmin"></a>'
    else:
        links += '<a id="Firs" href="{}Post?PostID={}"><img class="button" src="static/Theme/Navigation/Next.png" width="40vmin"></a>'.format(request.url_root, postList[post + 1][1])
        links += '<a id="Firs" href="{}Post?PostID={}"><img class="button" src="static/Theme/Navigation/NextNext.png" width="40vmin"></a>'.format(request.url_root, postList[len(postList) - 1][1])
    return links

if __name__ == "__main__":
    app.config.update(TEMPLATES_AUTO_RELOAD=True)
    port = 5000
    if (len(sys.argv) == 2):
        port = sys.argv[1]
    app.run(host="0.0.0.0", port=port, debug=True)

"""
How to video
<div class="videoBox">
<iframe width="560" height="315" src="https://www.youtube.com/embed/IvYLxrJj__s" frameborder="0" allowfullscreen></iframe>
</div>
"""
