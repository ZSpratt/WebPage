drop table if exists Users;

create table Users(
  name varchar,
  hashpass blob,
  email varchar,
  comms int
);
