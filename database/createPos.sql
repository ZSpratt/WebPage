drop table if exists Posts;
drop table if exists Tags;

create table Posts(
  post varchar,
  title varchar,
  description varchar,
  content varchar
);

create table Tags(
  post varchar,
  tag varchar
);