from PostListing import PostListing
from MCDatabase import addPost
from PIL import Image
import os

tags = "migrated, oldPosts"
oldPosts = PostListing()
x = 0
for post in oldPosts.posts:
    print("Working {} / {}".format(x, len(oldPosts.posts)))
    x += 1
    workingDir = "static/OldPosts/" + post + "/"

    dest = post
    title = open(workingDir + "Title.html", "r").read()
    files = [file for file in os.listdir(workingDir) if file.endswith('.png')]
    smFiles = []
    desc = open(workingDir + "Description.html", "r").read()

    destPath = "static/Posts/{}/".format(dest)
    while os.path.exists(destPath):
        dest += "C"
        destPath = "static/Posts/{}/".format(dest)
    os.makedirs("static/Posts/{}/".format(dest))

    for l in files:
        f = Image.open(workingDir + l)
        f.save("static/Posts/{}/{}".format(dest, l))

        img = Image.open("static/Posts/{}/{}".format(dest, l))
        w = int(img.size[0] * 0.25)
        h = int(img.size[1] * 0.25)
        img = img.resize((w, h), Image.ANTIALIAS)
        img.save("static/Posts/{}/small {}".format(dest, l))

    content = ""
    if content == "":
        i = 0
        for f in files:
            i += 1
            content += '<image src="static/Posts/{}/small {}" onclick="window.location = \'static/Posts/{}/{}\';"/>'.format(dest, l, dest, l)
            if i % (len(files)/2) == 0:
                content += "<br>\n"
    else:
        i = 0
        print(content)
        for f in files:
            print(f.filename)
            content = str.replace(content, "${}".format(i), 'static/Posts/{}/small {}" onclick="window.location = \'static/Posts/{}/{}\';"'.format(dest, f.filename, dest, f.filename))
            i += 1
        print(content)
    titleF = open("static/Posts/{}/Title.html".format(dest), "w")
    titleF.write(title)
    titleF.close()

    contentF = open("static/Posts/{}/Content.html".format(dest), "w")
    contentF.write(content)
    contentF.close()

    descF = open("static/Posts/{}/Description.html".format(dest), "w")
    descF.write(desc)
    descF.close()
    addPost(dest, tags.split(","))
