import sqlite3
from time import gmtime, strftime
import os
import sys
import re
import bcrypt
import hashlib
import requests
from flask_login import UserMixin, current_user
from WebSendmail import sendMail, sendMeMail
import _thread
import urllib

mypath = os.path.dirname(os.path.abspath(__file__))
realpath = os.path.dirname(os.path.relpath(__file__)) + "/"
# print(mypath)
db = sqlite3.connect(mypath + "/database/comments.db", check_same_thread=False)
us = sqlite3.connect(mypath + "/database/user.db", check_same_thread=False)
po = sqlite3.connect(mypath + "/database/posts.db", check_same_thread=False)
vi = sqlite3.connect(mypath + "/database/views.db", check_same_thread=False)


def generateComments(post=""):
    return """
    {}""".format(commentToString(post))


def commentToString(post, reply=0, results=None):
    gravatars = {}
    if results is None:
        results = db.execute('select rowid, * from Comments where reply = {} and post = "{}";'.format(reply, post))
    commentString = ""
    for r in results:
        commentString += '''
<div class="comment">
<div>
<div style="float:left; width:80px;">
'''
        user = getUser(r[2])
        email = user.email
        h = hashlib.md5(email.encode('utf-8')).hexdigest()
        url = "http://www.gravatar.com/avatar/" + h + "?d=404&r=x"
        back = "/static/defaultImage.png"
        print(url)
        if user.gravatar:
            commentString += '<image src = "{}" onerror="this.src = \'{}\'" max-width:80;><br>'.format(url, back)
        else:
            commentString += '<image src = "{}" max-width:80;><br>'.format(back)
        commentString += '''
</div>
<div style="float:right; width:calc(100% - 90px)">
    <div>
        <div class = "Name" id="comment{}" style="text-align:left; float:left;">{}<a href="#comment{}">#</a></div>
        <div class = "TimeDate" style="text-align:right; float:right;">{}</div>
        <div class="spacer" style="clear: both;"></div>
    </div> <br>
    <div class="commentText">{}</div><br>
    <div class="button" style="width:40px;" onclick="var x = document.getElementById('commentID{}'); if(x.style.display == 'none'){{x.style.display = 'block';}} else if(x.style.display == 'block'){{x.style.display = 'none';}}">Reply</div>
</div>
'''.format(r[0], r[2], r[0], r[3], r[4], r[0])
        usr = ""
        if not current_user.is_authenticated:
            usr = "Anonymous"
        else:
            usr = current_user.id
        commentString +='''
<center>
    <form method="post" id = "commentID{}" class = "commentBox" style="display:none;">
        <input type="hidden" name="reply" value="{}">
        <input type="hidden" name="post" value="{}">
        <input type="hidden" name="type" value="comment">
        <input type="hidden" name="name" value="{}">
        <textarea name="commentText" style="width:300px; height:150px; text-align:left"></textarea><br>
        <input type="submit" value="Submit"><br>
    </form>
</center>
<div class="spacer" style="clear: both;">
</div>
</div>
'''.format(r[0], r[0], r[5], usr)
        reps = db.execute("select rowid, * from Comments where reply = {};".format(r[0]))
        if reps is not None:
            commentString += '''
<div class="reply">
        {}
</div>
'''.format( commentToString(post, reply=r[0], results=reps))

        commentString += '</div>'

    return commentString

if __name__ == "__main__":
    print(generateComments())


def addComment(form):
    # print(form)
    name = form["name"]
    if name == "":
        name = "Anonymous"
    comment = form["commentText"]
    if comment == "":
        return
    cleaner = re.compile('<.*?>')
    comment = re.sub(cleaner, '', comment)
    db.execute('insert into Comments values (?, ?, ?, ?, ?)', (form["reply"], name, strftime("%Y-%m-%d %H:%M:%S", gmtime()), comment, form["post"]))
    res = db.execute('select max(rowid) from Comments;')
    res = res.fetchone()
    comID = res[0]
    db.commit()

    _thread.start_new_thread(sendMeMail, ("Comment Posted for " + form["post"] + " from " + name, comment + "<br><br><br> https:www.ZSpratt-arts.com/?PostID=" + form["post"] + "#comment" + str(comID)))


def register(name, password, email, gravatar):
    result = us.execute('select * from Users where name = ?;', (name, ))
    result = result.fetchone()
    if result is None and name != "Anonymous":
        phash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        # print(phash)
        email.strip().replace(" ", "")
        i = 1
        if not gravatar:
            i = 0
        us.execute('insert into Users values (?, ?, ?, ?, ?)', (name, phash, email, 0, i))
        us.commit()
        return None
    else:
        return "User Exists"

def updateUser(name, password, email, gravatar):
    result = us.execute('select * from Users where name = ?;', (name, ))
    result = result.fetchone()
    if result is None:
        return "No user by name {}".format(name)
    else:
        if bcrypt.checkpw(password.encode('utf-8'), result[1]):
            i = 1
            if not gravatar:
                i = 0
            us.execute("update Users set email = ?, useGravatar = ? where name = ?;", (email, i, name))
            us.commit()
        else:
            return "Wrong Password"

class User(UserMixin):
    id = ""
    email = ""
    comms = 1
    gravatar = False


def getUser(name):
    result = us.execute('select * from Users where name = ?;', (name, ))
    result = result.fetchone()
    if name == "Anonymous":
        user = User()
        user.id = "Anonymous"
        user.email = ""
        return user
    elif result is None:
        return None
    else:
        user = User()
        user.id = result[0]
        user.email = result[2]
        user.gravatar = result[4] == 1
        return user


def login(name, password):
    result = us.execute('select * from Users where name = ?;', (name, ))
    result = result.fetchone()
    if result is None:
        return False
    else:
        return bcrypt.checkpw(password.encode('utf-8'), result[1])


def getPostList(start = -1, limit = sys.maxsize):
    retList = []
    listing = po.execute("select rowid, post, title, content, description from Posts where rowid > ? order by rowid limit ?;", (start, limit))
    if listing is not None:
        for l in listing:
            retList.append(l)
    return retList

def postCount():
    result = po.execute('select count(*) from Posts;')
    result = result.fetchone()
    if result is None:
        return 0
    else:
        return result[0]

def getPost(name):
    result = po.execute('select rowid, * from Posts where post = ?;', (name, ))
    result = result.fetchone()
    if result is None:
        return None
    else:
        return result


def addPost(post, tags):
    postD = "static/Posts/" + post
    po.execute('insert into Posts values (?, ?, ?, ?);', (post, postD + "/Title.html", postD + "/Description.html", postD + "/Content.html"))
    po.commit()

    for tag in tags:
        if tag != "":
            po.execute("insert into Tags values (?, ?);", (post, tag.lower()))
    po.commit()


def searchPosts(tags):
    retList = []
    tagstr = ""
    for tag in tags:
        tagstr += 'Tags.tag like ? or '.format(tag.lower())
    search = 'select Posts.post, title, content, description, Posts.rowid from Posts natural join Tags where {} " " group by post order by Posts.rowid desc;'.format(tagstr)
    posts = po.execute(search, tags)
    for post in posts:
        retList.append(post)
    return retList


def updatePost(post, title, content, desc, tags):
    realpath = os.path.dirname(os.path.relpath(__file__))
    if realpath != '':
        realpath += "/"

    result = po.execute('select rowid, * from Posts where post = ?;', (post, ))
    result = result.fetchone()
    if result is not None:
        print("Realpath " + realpath)
        print(result[2])

        open(realpath + result[2], "w").write(title)
        open(realpath + result[3], "w").write(desc)
        open(realpath + result[4], "w").write(content)
        setTags(post, tags)
        return ("Edit Completed")
    return ("Edit Failed")


def setTags(post, tags):
    po.execute("delete from Tags where post == ?;", (post, ))
    po.commit()
    for tag in tags:
        if tag != "":
            po.execute("insert into Tags values (?, ?);", (post, tag.lower()))
    po.commit()


def getTags(post):
    tagList = ''
    search = 'select tag from Tags where post == "{}";'.format(post)
    tags = po.execute(search)
    for tag in tags:
        tagList += tag[0] + ", "
    return tagList


def addView(post, ip):
    if str(ip) != "127.0.0.1":
        result = vi.execute("select * from Views where post == ? and ip == ?;", (post, ip))
        result = result.fetchone()
        print(result)
        if result is None:
            vi.execute("insert into Views values (?, ?);", (post, ip))
            vi.commit()


def getViews(post):
    result = vi.execute("select count(*) from Views where post == ?;", (post, ))
    result = result.fetchone()
    print("Views : {}".format(result[0]))
    return result[0]
