import smtplib

from email.mime.text import MIMEText


def sendMeMail(subject, content):
    sendMail(subject, content, "zachman331@gmail.com@gmail.com")


def sendMail(subject, content, destination):
    #print("starting mail")
    mail = smtplib.SMTP("smtp.gmail.com", 587)
    mail.ehlo()

    #print("making message")
    message = MIMEText(content, 'html')
    message['From'] = "zachman331@gmail.com"
    message['To'] = destination
    message['subject'] = subject

    #print("logging in")
    mail.starttls()
    mail.login("zachman331@gmail.com", "NotMyPassword")
    # print("sending")
    mail.sendmail('supZSprattarts@gmail.com', destination, message.as_string())
    mail.close()
    #print("Mail Sent")

if __name__ == "__main__":
    subject = "test content"
    content = "test content"
    sendMail(subject, content)
