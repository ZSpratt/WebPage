import os
import sys
import time


class PostListing:

    def __init__(self):
        self.postPath = os.path.join(
            "./", os.path.dirname(os.path.relpath(__file__)), "static/OldPosts")
        if not os.path.exists(self.postPath + "/postOrder"):
            self.postListFile = open(self.postPath + "/postOrder", "w+")
        else:
            self.postListFile = open(self.postPath + "/postOrder", "r+")
        self.posts = []
        self.updateLists()

    def __getitem__(self, key):
        return self.postPath + "/" + self.posts[key]

    def __len__(self):
        return len(self.posts)

    def updateLists(self):
        for line in self.postListFile:
            self.posts.append(line.rstrip())
        fList = os.listdir(self.postPath)
        if len(fList) - 1 != len(self.posts):
            for l in fList:
                if l not in self.posts:
                    if os.path.isdir(self.postPath + "/" + l):
                        self.posts.append(l)
                        self.postListFile.write(l + "\n")
        self.postListFile.close()
        self.postListFile = open(self.postPath + "/postOrder", "r+")


if __name__ == "__main__":
    pl = PostListing()
    print(pl.posts)
